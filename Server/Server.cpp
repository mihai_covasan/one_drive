#include <iostream>
#include <string>
#include <fstream>
#include <array>
#include <sstream>
#include <iomanip>

#include "TcpSocket.h"
#include "database.h"
#include "Utils.h"


enum class CustomMsgTypes : uint32_t
{
	FilesList,
	RemoveFile,
	SendFile,
	Authenticate,
	Register,
	CheckExistingFolder,
	GetLocal,
};

std::string getTimeStr() {
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::ostringstream oss;
	oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
	auto str = oss.str();
	return str;
}

std::string deserializeCall(const std::string& data, Database& data_base, Utils& file_excchange_)
{
	CustomMsgTypes messageType = (CustomMsgTypes)(data[0] - '0');
	//std::cout << (uint32_t)messageType << '\n';
	std::string processedData = data.substr(2, data.size());
	std::cout << "Processing... \n";

	switch (messageType)
	{
	case CustomMsgTypes::Authenticate:
	{
		std::cout << "Entered in switch\n";
		std::string response;
		//client.ReceiveFile("isahiads");
		if (data_base.CheckIfUserExists(processedData)) {
			//response = processedData + " connected" + "|" + getTimeStr() + '\n';
			file_excchange_.OpenDirectory(processedData);
			response = "True";
			std::cout << "The response that is sent is: " + response << std::endl;
		}
		else {
			response = "False";
			std::cout << "The response that is sent is: " + response << std::endl;
		}
		return response;
		break;
	}
	case CustomMsgTypes::Register:
	{
		//std::cout << "Entered in switch\n";
		std::string response;
		if (!data_base.CheckIfUserExists(processedData)) {
			//	std::cout << "The user " << processedData << " has registered successfully!";
			data_base.AddUser(processedData);
			file_excchange_.MakeDirectory(processedData);
			file_excchange_.MakeDirectoryClient(processedData);
			file_excchange_.MakeDirectoryTrash(processedData);
			response = "True";
		}
		else {
			//	std::cout << "The user " << processedData << " exists or something is wrong!";
			response = "False";
		}

		return response;
		break;


	}
	case CustomMsgTypes::FilesList:
	{
		std::cout << "Entered in switch2\n";

		//things to do when getting the filesList
		break;
	}
	case CustomMsgTypes::SendFile:
	{
		std::cout << "Entered in switch3\n";
		std::cout << processedData;
		file_excchange_.WriteFile(processedData, getTimeStr());
		return "File Download \n";
		break;
	}

	case CustomMsgTypes::RemoveFile:
	{
		std::cout << "Entered in switch\n";
		// things to do when removing the folder
		break;
	}
	case CustomMsgTypes::CheckExistingFolder:
	{
		std::cout << "Entered in switch\n";
		// things to do when checking the existing folder
		break;
	}
	case CustomMsgTypes::GetLocal:
	{
		std::cout << "Entered in switchAAAAAAAAAAA\n";
		std::cout << processedData;
		file_excchange_.WriteFileLocal(processedData, getTimeStr());
		return "File Download \n";
		break;
	}
	}
}

int main(int argc, char* argv[], char* environment[])
{
	std::cout << "Starting server" << std::endl;
	TcpSocket listener;
	Database data_base_;
	Utils file_exchange_;
	data_base_.OpenDataBase();
	data_base_.CreateTable();
	listener.Listen(27015);
	while (true)
	{
		std::cout << "Waiting for client to connect" << std::endl;
		TcpSocket client = listener.Accept();

		// receive
		std::array<char, 32768> receiveBuffer;
		int recieved;
		client.Receive(receiveBuffer.data(), receiveBuffer.size(), recieved);
		auto response = std::string(receiveBuffer.data(), 0, recieved);
		response = deserializeCall(response, data_base_, file_exchange_);
		client.Send(response.c_str(), response.size());

		//std::cout << "Sending: " << response.size() + 1 << " bytes" << std::endl;
	}

	return 0;
}