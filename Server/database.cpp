  #include "database.h"
#include <sstream>

Database::Database() {}

static int callback(void* data, int argc, char** argv, char** azColName)
{
    int i;
    std::string output_status;
    std::stringstream ss;
    std::string output;
    int status;
    //fprintf(stderr, "%s: ", (const char*)data);

    for (i = 0; i < argc; i++) {
        //printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        ss << argv[i];
    }
    ss >> output;
    if (output.empty()) {
        //std::cout << "User not exists";
        status = 0;
    }
    else {
        //std::cout << "User exists";
        status = 1;
    }

    printf("\n");
    return status;
}


bool Database::OpenDataBase() {
    int rc;
    bool status;
    rc = sqlite3_open("database_users.db", &db);

    if (rc) {
        std::cout << (stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        status = false;
    }
    else {
        std::cout << (stdout, "Opened database successfully\n");
        status = true;
    }

    return status;
}


bool Database::CreateTable() {
    std::string sql = "CREATE TABLE PERSON("
        "ID INTEGER PRIMARY KEY     AUTOINCREMENT, "
        "NAME           TEXT    NOT NULL ); ";

    bool status;
    int exit = 0;
    char* messaggeError;
    exit = sqlite3_exec(db, sql.c_str(), NULL, 0, &messaggeError);

    if (exit != SQLITE_OK) {
        std::cout << "Table already created" << std::endl;
        sqlite3_free(messaggeError);
        status = false;
    }
    else {
        std::cout << "Table created Successfully" << std::endl;
        status = true;
    }

    return status;

}

bool Database::AddUser(std::string name) {
    char* messaggeError;
    int exit = 0;
    bool status;

    std::string sql = "INSERT INTO PERSON (NAME) \n"
        "VALUES ( '" + name + "' );";

    exit = sqlite3_exec(db, sql.c_str(), NULL, 0, &messaggeError);
    if (exit != SQLITE_OK) {
        std::cout << "Error Insert" << std::endl;
        status = false;
        sqlite3_free(messaggeError);
    }
    else {
        std::cout << "User inserted sucesfully!" << std::endl;
        status = true;
    }

    return status;

}

bool Database::CheckIfUserExists(std::string name) {

    std::string data("CALLBACK FUNCTION");
    std::string sql = "SELECT NAME from PERSON where NAME = '" + name + "';";
    std::string output;
    bool status;

    int rc = sqlite3_exec(db, sql.c_str(), callback, (void*)data.c_str(), NULL);

    if (rc == 0) {
        //std::cout  << "User not exist" << std::endl;
        status = false;
    }
    else {
        //std::cout << "User exist" << std::endl;
        status = true;
    }

    return status;

}

Database::~Database() {
    sqlite3_close(db);
}