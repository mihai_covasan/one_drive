#include "Utils.h"
#include <iomanip>
#include <windows.h>
#define CreateDirectory

Utils::Utils() {
	std::string data_storage = ".\\DataStorage";
	CreateDirectory(data_storage.c_str(), NULL);
}

void Utils::WriteFile(std::string file_data, std::string date_time) {

	std::cout << "Downloading file... \n";
	// int size_of_name = std::stoi(file_data.substr(file_data.size()));
	// std::string name = file_data.substr(file_data.size() - 1, file_data.size() - (1 + size_of_name));
	// std::cout << size_of_name << std::endl << name;

	std::ofstream sout("DataStorage\\" + this->temporary_name + "\\" + date_time + ".txt");
	sout << file_data;
	sout.close();

	std::ofstream out("..\\ClientApp\\DataStorage\\" + this->temporary_name + "\\" + date_time + ".txt");
	out << file_data;
	out.close();
}

void Utils::WriteFileLocal(std::string file_data, std::string date_time) {

	std::cout << "Downloading file... \n";

	std::ofstream out("..\\ClientApp\\DataStorage\\" + this->temporary_name + "\\" + date_time + ".txt");
	out << file_data;
	out.close();
}

void Utils::OpenDirectory(std::string name) {

	this->temporary_name = name;

}

bool Utils::MakeDirectory(std::string name) {

	this->temporary_name = name;
	bool status;
	std::string output_folder = ".\\DataStorage\\" + name;
	std::cout << output_folder;
	if (CreateDirectory(output_folder.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		status = true;
	}
	else
	{
		status = false;
	}

	return status;

}

bool Utils::MakeDirectoryClient(std::string name) {

	this->temporary_name = name;
	bool status;
	std::string output_folder = "..\\ClientApp\\DataStorage\\" + name;
	std::cout << output_folder;
	if (CreateDirectory(output_folder.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		status = true;
	}
	else
	{
		status = false;
	}

	return status;

}
bool Utils::MakeDirectoryTrash(std::string name) {

	this->temporary_name = name;
	bool status;
	std::string output_folder = ".\\Trash\\" + name;
	std::cout << output_folder;
	if (CreateDirectory(output_folder.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		status = true;
	}
	else
	{
		status = false;
	}

	return status;

}