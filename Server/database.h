#include <iostream>
#include <string>
#include "sqlite3.h"

class Database {
public:
    bool OpenDataBase();
    bool CreateTable();
    bool AddUser(std::string name);
    bool CheckIfUserExists(std::string name);
    Database();
    ~Database();
private:
    sqlite3* db;

};
