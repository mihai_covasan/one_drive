#pragma once
#include <iostream>
#include <fstream>
#include <string>

//#include <experimental/filesystem>

class Utils
{
public:
	void WriteFile(std::string file_data, std::string date_time);
	void WriteFileLocal(std::string file_data, std::string date_time);
	bool MakeDirectory(std::string name);
	bool MakeDirectoryClient(std::string name);
	bool MakeDirectoryTrash(std::string name);
	void OpenDirectory(std::string name);
	std::string temporary_name;

	Utils();
};
